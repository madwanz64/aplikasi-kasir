@extends('layouts.master')

@section('title')
    Dashboard | Cashier
@endsection

@push('styles')
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@php
function tanggalFormat($date)
{
    $day = [1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu', 'Minggu'];
    $month = [1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

    $hari = $day[$date->format('N')];
    $tanggal = $date->format('d') . ' ' . $month[$date->format('n')] . ' ' . $date->format('Y');
    $jam = $date->format('H') . ':' . $date->format('i');

    return $hari . ', ' . $tanggal . ' ' . $jam;
}
@endphp

@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
        <a href="{{ route('transaksi.create') }}" class="btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-cart-plus fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Tambah Transaksi</div>
        </a>
    </div>
    <div class="row d-flex">
        <div class="order-md-1 col-md-4">

            <!-- Earnings (Monthly) Card Example -->
            <div class="mb-3">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Barang Terjual (Hari ini)</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                    {{ number_format($total_barang, 0, ',', '.') }} Barang</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-box fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="mb-3">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Banyak Transaksi (Hari ini)</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                    {{ number_format($transaksi_pembelian->count(), 0, ',', '.') }}
                                    Transaksi</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-shopping-cart fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Earnings (Monthly) Card Example -->
            <div class="mb-3">
                <div class="card border-left-info shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Total Transaksi (Hari ini)</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">Rp
                                    {{ number_format($transaksi_pembelian->sum('total_harga'), 0, ',', '.') }}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="order-md-0 col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Daftar Transaksi Pembelian Barang (Hari ini)</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th>Waktu Transaksi</th>
                                    <th>Total Transaksi (Rp)</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th>Waktu Transaksi</th>
                                    <th>Total Transaksi (Rp)</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($transaksi_pembelian as $item)
                                    <tr>
                                        <td class="text-center">{{ $item->id }}</td>
                                        <td>{{ tanggalFormat($item->created_at) }}</td>
                                        <td class="text-right">{{ number_format($item->total_harga, 0, ',', '.') }}
                                        </td>
                                        <td class="text-center"><a
                                                href="{{ route('transaksi.show', ['transaksi' => $item->id]) }}"
                                                class="btn btn-sm btn-info">Detail</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">

    </div>
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                "order": [
                    [0, "desc"]
                ],
                columnDefs: [{
                    orderable: false,
                    targets: 1,
                }, {
                    orderable: false,
                    targets: 3,
                }]
            });
        });
    </script>
@endpush
