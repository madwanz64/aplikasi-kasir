@extends('layouts.master')

@section('title')
    Tambah Transaksi | Cashier
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Transaksi</h1>
        <a href="{{ route('dashboard') }}" class="btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-chevron-circle-left fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Kembali</div>
        </a>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Transaksi Baru</h6>
        </div>
        <div class="card-body">
            <form action="{{ route('transaksi.store') }}" method="post">
                @csrf
                <div id="form_field">
                    <div class="row d-flex" id="row1">
                        <div class="col-12 col-md-5">
                            <div class="form-group">
                                <input class="form-control formBarang namaBarang" id="barang1" name="barang[]"
                                    list="nama_barang" placeholder="Pilih barang..." onchange="resetIfInvalid(this);"
                                    autocomplete="off" required>
                                <datalist id="nama_barang">
                                    @foreach ($barang as $item)
                                        <option value="{{ $item->id }} - {{ $item->nama_barang }}"></option>
                                    @endforeach
                                </datalist>
                            </div>
                        </div>
                        <div class="col-8 col-md-2">
                            <div class="form-group">
                                <input type="number" min="0" class="form-control formBarang kuantitas" id="kuantitas1"
                                    name="kuantitas[]" placeholder="Kuantitas" required>
                            </div>
                        </div>
                        <div class="order-4 order-md-3 col-6 col-md-2">
                            <div class="form-group">
                                <input type="number" value="0" class="form-control formBarang" id="harga1"
                                    placeholder="Harga Satuan" readonly>
                            </div>
                        </div>
                        <div class="order-5 order-md-4 col-6 col-md-2">
                            <div class="form-group">
                                <input type="number" value="0" class="form-control formBarang subtotal" id="subtotal1"
                                    placeholder="Subtotal" readonly>
                            </div>
                        </div>
                        <div class="order-3 order-md-5 col-4 col-md-1">
                            <button type="button" id="add" class="btn btn-primary w-100">&plus;
                            </button>
                        </div>

                    </div>
                </div>
                <div class="row d-flex justify-content-end">
                    <div class="col-md-3">
                        <input type="text" id="totalTransaksi" value="0" class="form-control mb-3" placeholder="Total"
                            readonly>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary w-100 mb-3">Tambah Transaksi</button>
            </form>

        </div>
    </div>
@endsection


@push('scripts')
    <script>
        function resetIfInvalid(el) {
            //just for beeing sure that nothing is done if no value selected
            if (el.value == "")
                return;
            var options = el.list.options;
            for (var i = 0; i < options.length; i++) {
                if (el.value == options[i].value)
                    //option matches: work is done
                    return;
            }
            //no match was found: reset the value
            el.value = "";
        }

        var barang = JSON.parse(JSON.stringify(<?php echo $barang; ?>))
        var harga = [];
        for (let i = 0; i < barang.length; i++) {
            index = barang[i]['id'];
            harga[index] = barang[i]['harga_satuan']
        };

        $(document).ready(function() {
            var i = 1;
            var exist = [1];

            function arrayRemove(arr, value) {

                return arr.filter(function(ele) {
                    return ele != value;
                });
            }
            console.log(exist)

            $('#add').click(function() {
                i++
                exist.push(i)
                $('#form_field').append(`<div class="row d-flex" id="row` + i + `">
                    <div class="col-12 col-md-5">
                        <div class="form-group">
                            <input class="form-control formBarang namaBarang" id="barang` + i + `" name="barang[]" list="nama_barang"placeholder="Pilih barang..." onchange="resetIfInvalid(this);" autocomplete="off" required>
                            <datalist id="nama_barang">
                        @foreach ($barang as $item)
                            <option value="{{ $item->id }} - {{ $item->nama_barang }}"></option>
                        @endforeach
                        </datalist>
                    </div>
                    </div>
                    <div class="col-8 col-md-2">
                    <div class="form-group">
                        <input type="number" min="0" class="form-control formBarang kuantitas" id="kuantitas` + i + `" name="kuantitas[]" placeholder="Kuantitas" required>
                    </div>
                    </div>
                    <div class="order-4 order-md-3 col-6 col-md-2">
                    <div class="form-group">
                        <input type="number" min="0" class="form-control formBarang" value="0" id="harga` + i + `" placeholder="Harga Satuan" readonly>
                    </div>
                    </div>
                    <div class="order-5 order-md-4 col-6 col-md-2">
                    <div class="form-group">
                        <input type="number" min="0" class="form-control formBarang subtotal" value="0" id="subtotal` +
                    i + `" placeholder="Subtotal" readonly>
                    </div>
                    </div>
                    <div class="order-3 order-md-5 col-4 col-md-1">
                    <button type="button" class="btn btn-danger w-100 btn_remove" id="button` + i + `">&times;</button>
                    </div>
                    </div>`);
            });

            $(document).on('change', '.namaBarang', function() {
                index = $(this).val().split(' ')[0]
                id = $(this).attr('id').match(/(\d+)/)[0]
                kuantitas = $('#kuantitas' + id + '').val()
                $('#harga' + id + '').val(harga[index])
                $('#subtotal' + id + '').val($('#harga' + id + '').val() * kuantitas)

                var total = parseInt(0)
                for (let i = 0; i < exist.length; i++) {
                    total += parseInt($('#subtotal' + exist[i] + '').val())
                }
                $('#totalTransaksi').val(total)
            })

            $(document).on('change', '.kuantitas', function() {
                kuantitas = $(this).val()
                id = $(this).attr('id').match(/(\d+)/)[0]
                $('#subtotal' + id + '').val($('#harga' + id + '').val() * kuantitas)

                var total = parseInt(0)
                for (let i = 0; i < exist.length; i++) {
                    total += parseInt($('#subtotal' + exist[i] + '').val())
                }
                $('#totalTransaksi').val(total)
            })



            $(document).on('click', '.btn_remove', function() {
                var button_id = $(this).attr("id").match(/(\d+)/)[0];
                exist = arrayRemove(exist, button_id)
                $('#row' + button_id + '').remove();
            });
        });
    </script>
@endpush
