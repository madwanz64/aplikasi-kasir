@extends('layouts.master')

@section('title')
    Transaksi #{{ $id }} | Cashier
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Transaksi #{{ $id }}</h1>
        <a href="{{ route('transaksi.print', ['transaksi_id' => $id]) }}" class="btn btn-sm btn-danger shadow-sm"><i
                class="fas fa-download fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Unduh PDF</div>
        </a>
    </div>
    <div class="row">

    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Daftar Barang</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center">
                            <th>No</th>
                            <th>Nama Barang</th>
                            <th>Kuantitas</th>
                            <th>Harga Satuan</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan="4" class="text-center">Total Transaksi</th>
                            <th class="text-right"> Rp {{ number_format($total, 0, ',', '.') }}</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($transaksi as $key => $value)
                            <tr>
                                <td class="text-center">{{ $key + 1 }}</td>
                                <td>{{ $value->masterBarang->nama_barang }}</td>
                                <td class="text-center">{{ $value->jumlah }}</td>
                                <td class="text-right">{{ number_format($value->harga_satuan, 0, ',', '.') }}</td>
                                <td class="text-right">
                                    {{ number_format($value->jumlah * $value->harga_satuan, 0, ',', '.') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
