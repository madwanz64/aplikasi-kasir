@extends('layouts.master')

@section('title')
    Data Transaksi | Cashier
@endsection

@push('styles')
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@php
function tanggalFormat($date)
{
    $day = [1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu', 'Minggu'];
    $month = [1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

    $hari = $day[$date->format('N')];
    $tanggal = $date->format('d') . ' ' . $month[$date->format('n')] . ' ' . $date->format('Y');
    $jam = $date->format('H') . ':' . $date->format('i');

    return $hari . ', ' . $tanggal . ' ' . $jam;
}
@endphp

@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Transaksi</h1>
        <a href="{{ route('transaksi.create') }}" class="btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-cart-plus fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Tambah Transaksi</div>
        </a>
    </div>

    <div class="card shadow mb-4">
        <!-- Card Header - Accordion -->
        <a href="#filterCard" class="d-block card-header py-3" data-toggle="collapse" role="button">
            <h6 class="m-0 font-weight-bold text-primary">Filter Transaksi</h6>
        </a>
        <!-- Card Content - Collapse -->
        <div class="collapse show" id="filterCard">
            <div class="card-body">
                <form action="{{ route('transaksi.index') }}" method="get">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="font-weight-bold text-primary">Berdasarkan Tanggal</h6>
                            <div class="form-group">
                                <input type="date" name="date" value="{{ request('date') }}" class="form-control">
                            </div>

                            <h6 class="font-weight-bold text-primary">Berdasarkan Total Harga</h6>
                            <div class="form-group">
                                <input type="number" name="min" min="0" value="{{ request('min') }}"
                                    class="form-control" placeholder="Harga Minimal">
                            </div>

                            <div class="form-group">
                                <input type="number" name="max" min="0" value="{{ request('max') }}"
                                    class="form-control" placeholder="Harga Maksimal">
                            </div>

                        </div>
                        <div class="col-md-6">
                            <h6 class="font-weight-bold text-primary">Berdasarkan Barang</h6>
                            <div id="filterBarang">
                                <div class="row" id="row1">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input class="form-control formBarang namaBarang" name="barang[]"
                                                list="nama_barang"
                                                value="{{ request('barang') != null ? request('barang')[0] : '' }}"
                                                onchange="resetIfInvalid(this);">
                                            <datalist id="nama_barang">
                                                @foreach ($barang as $item)
                                                    <option value="{{ $item->id }} - {{ $item->nama_barang }}">
                                                    </option>
                                                @endforeach
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="button" id="tambahField" class="btn btn-primary w-100 mb-3">&plus;
                                        </button>
                                    </div>
                                </div>

                                @if (request('barang') != null)
                                    @php
                                        $countBarang = count(request('barang'));
                                    @endphp

                                    @for ($i = 1; $i < $countBarang; $i++)
                                        <div class="row" id="row{{ $i + 1 }}">
                                            <div class="col-10">
                                                <div class="form-group">
                                                    <input class="form-control formBarang namaBarang" name="barang[]"
                                                        list="nama_barang" value="{{ request('barang')[$i] }}"
                                                        onchange="resetIfInvalid(this);">
                                                    <datalist id="nama_barang">
                                                        @foreach ($barang as $item)
                                                            <option
                                                                value="{{ $item->id }} - {{ $item->nama_barang }}">
                                                            </option>
                                                        @endforeach
                                                    </datalist>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <button type="button" id="{{ $i + 1 }}"
                                                    class="btn btn-danger w-100 mb-3 kurangField">&minus;
                                                </button>
                                            </div>
                                        </div>
                                    @endfor
                                @else
                                    @php
                                        $countBarang = 1;
                                    @endphp
                                @endif
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-primary w-100">Cari</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Transaksi</h6>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Waktu Transaksi</th>
                            <th>Total Transaksi (Rp)</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Waktu Transaksi</th>
                            <th>Total Transaksi (Rp)</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($transaksi_pembelian as $item)
                            <tr>
                                <td class="text-center">{{ $item->id }}</td>
                                <td>{{ tanggalFormat($item->created_at) }}</td>
                                <td class="text-right">{{ number_format($item->total_harga, 0, ',', '.') }}</td>
                                <td class="text-center"><a
                                        href="{{ route('transaksi.show', ['transaksi' => $item->id]) }}"
                                        class="btn btn-sm btn-info">Detail</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: 1,
                }, {
                    orderable: false,
                    targets: 3
                }, {
                    type: "num-fmt",
                    targets: 2
                }]
            });
        });
    </script>

    <script>
        function resetIfInvalid(el) {
            //just for beeing sure that nothing is done if no value selected
            if (el.value == "")
                return;
            var options = el.list.options;
            for (var i = 0; i < options.length; i++) {
                if (el.value == options[i].value)
                    //option matches: work is done
                    return;
            }
            //no match was found: reset the value
            el.value = "";
        }
        var i = <?= $countBarang ?>

        $(document).ready(function() {
            $('#tambahField').on('click', function() {
                i++
                $('#filterBarang').append(`<div class="row" id="row` + i + `">
                                    <div class="col-10">
                                        <div class="form-group">
                                            <input class="form-control formBarang namaBarang" name="barang[]"
                                                list="nama_barang" onchange="resetIfInvalid(this);" required>
                                            <datalist id="nama_barang">
                                                @foreach ($barang as $item)
                                                    <option value="{{ $item->id }} - {{ $item->nama_barang }}">
                                                    </option>
                                                @endforeach
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="col-2">
                                        <button type="button" id="` + i + `"
                                            class="btn btn-danger w-100 mb-3 kurangField">&minus;</button>
                                    </div>
                                </div>`)
            })

            $(document).on('click', '.kurangField', function() {
                var id = $(this).attr('id')
                console.log()
                $('#row' + id + '').remove()
            })
        })
    </script>
@endpush
