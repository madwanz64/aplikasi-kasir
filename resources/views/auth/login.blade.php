@extends('layouts.auth')

@section('title')
    Login Form | Cashier
@endsection

@section('form')
    <form class="user" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-group">
            <input id="email" class="form-control form-control-user @error('email') is-invalid @enderror" type="email"
                name="email" value="{{ old('email') }}" placeholder="Masukkan alamat email..." required autofocus />
        </div>
        <div class="form-group">
            <input id="password" class="form-control form-control-user" type="password" name="password"
                placeholder="Kata sandi" required autocomplete="current-password" />
        </div>
        @error('email')
            {{ $message }}
        @enderror
        <div class="form-group">
            <div class="custom-control custom-checkbox small">
                <input type="checkbox" class="custom-control-input" id="customCheck">
                <label class="custom-control-label" for="customCheck">Remember
                    Me</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            Login
        </button>
        <hr>
        <div class="text-center">
            <a class="small" href="{{ route('register') }}">Buat Akun Baru!</a>
        </div>
    </form>
@endsection
