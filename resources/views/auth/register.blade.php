@extends('layouts.auth')

@section('title')
    Buat Akun Baru | Cashier
@endsection

@section('form')
    <form class="user" method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form-group">
            <div class="mb-3 mb-sm-0">
                <input id="name" class="form-control form-control-user" type="text" name="name" value="{{ old('name') }}"
                    placeholder="Nama lengkap" required autofocus />
            </div>
        </div>
        <div class="form-group">
            <input id="email" class="form-control form-control-user @error('email') is-invalid @enderror " type="email"
                name="email" value="{{ old('email') }}" placeholder="Alamat email" required />
            @error('email')
                <div class="invalid-feedback">
                    Email sudah terdaftar
                </div>
            @enderror
        </div>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <input id="password" class="form-control form-control-user" type="password" name="password"
                    placeholder="Kata sandi" required autocomplete="new-password" />
            </div>
            <div class="col-sm-6">
                <input id="password_confirmation" class="form-control form-control-user" type="password"
                    name="password_confirmation" placeholder="Ketik ulang kata sandi" required />
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-user btn-block">
            Buat akun baru
        </button>
        <hr>
        <div class="text-center">
            <a class="small" href="{{ route('login') }}">Sudah punya akun? Login!</a>
        </div>
    </form>
@endsection
