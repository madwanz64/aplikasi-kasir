@extends('layouts.master')

@section('title')
    Data Barang #{{ $barang->id }} | Cashier
@endsection

@php
function tanggalFormat($date)
{
    $day = [1 => 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu', 'Minggu'];
    $month = [1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

    $hari = $day[$date->format('N')];
    $tanggal = $date->format('d') . ' ' . $month[$date->format('n')] . ' ' . $date->format('Y');
    $jam = $date->format('H') . ':' . $date->format('i');

    return $hari . ', ' . $tanggal . ' ' . $jam;
}
@endphp

@push('styles')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush


@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data {{ $barang->nama_barang }}</h1>
        <button type="button" class="btn btn-sm btn-primary updateBarang" data-toggle="modal" data-target="#updateBarang"
            data-id="{{ $barang->id }}" data-nama="{{ $barang->nama_barang }}"
            data-harga="{{ $barang->harga_satuan }}">
            <i class="fas fa-edit fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Update Barang</div>
        </button>
    </div>

    <div class="row d-flex">
        <div class="order-lg-1 col-lg-4">
            <div class="row">
                <div class="col-md-6 col-lg-12">
                    <div class="card border-left-success shadow py-2 mb-3">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                        Total Pendapatan</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        Rp {{ number_format($data['total'], 0, ',', '.') }}</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-box fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-12">
                    <div class="card border-left-warning shadow py-2 mb-3">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                        Jumlah Barang Terjual</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        {{ number_format($data['jumlah'], 0, ',', '.') }} buah</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-box fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-12">
                    <div class="card border-left-danger shadow py-2 mb-3">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                        Jumlah Transaksi</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                        {{ number_format(count($transaksi), 0, ',', '.') }} transaksi</div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-box fa-2x text-gray-300"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <div class="order-lg-0 col-lg-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Transaksi {{ $barang->nama_barang }}</h6>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>#ID</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Jumlah</th>
                                    <th>Harga Satuan</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr class="text-center">
                                    <th>#ID</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Jumlah</th>
                                    <th>Harga Satuan</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($transaksi as $item)
                                    <tr>
                                        <td class="text-center"><a
                                                href="{{ route('transaksi.show', ['transaksi' => $item->transaksi_pembelian_id]) }}">{{ $item->transaksi_pembelian_id }}</a>
                                        </td>
                                        <td>{{ tanggalFormat($item->transaksiPembelian->created_at) }}</td>
                                        <td class="text-center">{{ $item->jumlah }}</td>
                                        <td class="text-right">Rp
                                            {{ number_format($item->harga_satuan, 0, ',', '.') }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('modal')
    <div class="modal fade" id="updateBarang" tabindex="-1" aria-labelledby="updateBarangLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content shadow mb-4">
                <div class="card">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Update Barang</h6>
                    </div>
                    <div class="card-body">
                        <form id="formUpdateBarang" action="{{ route('barang.update', ['barang' => $barang->id]) }}"
                            method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="namaBarang">Nama Barang</label>
                                <input type="text" name="nama_barang" class="form-control" id="namaBarang"
                                    oninvalid="this.setCustomValidity('Nama Barang tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <div class="form-group">
                                <label for="hargaSatuan">Harga Satuan</label>
                                <input type="number" name="harga_satuan" min="0" class="form-control" id="hargaSatuan"
                                    oninvalid="this.setCustomValidity('Harga Satuan tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Update Barang</button>

                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.updateBarang').on('click', function() {
                var id = $(this).attr('data-id')
                $('#namaBarang').val($(this).attr('data-nama'))
                $('#hargaSatuan').val($(this).attr('data-harga'))
            })
        })
    </script>
    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: 1,
                }, {
                    orderable: false,
                    targets: 3,
                }],
                "order": [
                    [0, "desc"]
                ]
            });
        });
    </script>
@endpush
