@extends('layouts.master')

@section('title')
    Tambah Barang | Cashier
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Barang</h1>
        <a href="{{ route('barang.index') }}" class="btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-list fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Data Barang</div>
        </a>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Barang Baru</h6>
        </div>
        <div class="card-body">
            <form action="{{ route('barang.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="namaBarang">Nama Barang</label>
                    <input type="text" name="nama_barang" class="form-control"
                        oninvalid="this.setCustomValidity('Nama Barang tidak boleh kosong')"
                        oninput="this.setCustomValidity('')" required>
                </div>
                <div class="form-group">
                    <label for="hargaSatuan">Harga Satuan</label>
                    <input type="number" name="harga_satuan" min="0" class="form-control"
                        oninvalid="this.setCustomValidity('Harga Satuan tidak boleh kosong')"
                        oninput="this.setCustomValidity('')" required>
                </div>
                <button type="submit" class="btn btn-primary w-100 mb-3">Tambah Barang Baru</button>
            </form>

        </div>
    </div>
@endsection

@push('scripts')
@endpush
