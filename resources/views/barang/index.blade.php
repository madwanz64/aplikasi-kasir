@extends('layouts.master')

@section('title')
    Data Barang | Cashier
@endsection

@push('styles')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush


@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Barang</h1>
        <button type="button" class="btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#tambahBarang">
            <i class="fas fa-plus fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Tambah Barang</div>
        </button>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Barang</h6>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Nama Barang</th>
                            <th>Harga Satuan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Nama Barang</th>
                            <th>Harga Satuan</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($barang as $item)
                            <tr>
                                <td class="text-center">{{ $item->id }}</td>
                                <td>{{ $item->nama_barang }}</td>
                                <td class="text-right">Rp {{ number_format($item->harga_satuan, 0, ',', '.') }}</td>
                                <td class="text-center text-nowrap">
                                    <a href="{{ route('barang.show', ['barang' => $item->id]) }}"
                                        class="btn btn-sm btn-info">Detail</a>
                                    <button type="button" class="btn btn-sm btn-outline-primary updateBarang"
                                        data-toggle="modal" data-target="#updateBarang" data-id="{{ $item->id }}"
                                        data-nama="{{ $item->nama_barang }}" data-harga="{{ $item->harga_satuan }}"
                                        data-url="{{ route('barang.update', ['barang' => $item->id]) }}">
                                        Edit
                                    </button>
                                    <form action="{{ route('barang.destroy', ['barang' => $item->id]) }}" method="POST"
                                        class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="tambahBarang" tabindex="-1" aria-labelledby="tambahBarangLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content shadow mb-4">
                <div class="card">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Tambah Barang</h6>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('barang.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="namaBarang">Nama Barang</label>
                                <input type="text" name="nama_barang" class="form-control"
                                    oninvalid="this.setCustomValidity('Nama Barang tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <div class="form-group">
                                <label for="hargaSatuan">Harga Satuan</label>
                                <input type="number" name="harga_satuan" min="0" class="form-control"
                                    oninvalid="this.setCustomValidity('Harga Satuan tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Tambah Barang</button>

                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updateBarang" tabindex="-1" aria-labelledby="updateBarangLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content shadow mb-4">
                <div class="card">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Update Barang</h6>
                    </div>
                    <div class="card-body">
                        <form id="formUpdateBarang" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="namaBarang">Nama Barang</label>
                                <input type="text" name="nama_barang" class="form-control" id="namaBarang"
                                    oninvalid="this.setCustomValidity('Nama Barang tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <div class="form-group">
                                <label for="hargaSatuan">Harga Satuan</label>
                                <input type="number" name="harga_satuan" min="0" class="form-control" id="hargaSatuan"
                                    oninvalid="this.setCustomValidity('Harga Satuan tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Update Barang</button>

                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.updateBarang').on('click', function() {
                var id = $(this).attr('data-id')
                $('form#formUpdateBarang').attr('action', $(this).attr('data-url'))
                $('#namaBarang').val($(this).attr('data-nama'))
                $('#hargaSatuan').val($(this).attr('data-harga'))
            })
        })
    </script>

    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: 3
                }]
            });
        });
    </script>
@endpush
