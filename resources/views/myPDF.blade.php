<!DOCTYPE html>
<html>

<head>
    <title>Transaksi #{{ $transaksi->id }}</title>
    <style>
        html {
            margin: 0;
            padding: 0;
            font-size: 10px;
        }

        body {
            margin: 0 10pt;
        }

    </style>
</head>

<body style="font-family: monospace">
    <h1>{{ $title }}</h1>
    <p>{{ $transaksi->created_at }}</p>
    <br>
    <table style="width:100vw;">
        <thead>
            <tr>
                <th>Item</th>
                <th style="text-align:center">Qty</th>
                <th style="text-align:right">Harga</th>
                <th style="text-align:right">Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($barang as $item)
                <tr>
                    <td>{{ $item->masterBarang->nama_barang }}</td>
                    <td style="text-align: center">{{ $item->jumlah }}</td>
                    <td style="text-align: right">{{ number_format($item->harga_satuan, 0, ',', '.') }}</td>
                    <td style="text-align: right">
                        {{ number_format($item->jumlah * $item->harga_satuan, 0, ',', '.') }}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Total</th>
                <th style="text-align: right">{{ number_format($transaksi->total_harga, 0, ',', '.') }}</th>
            </tr>
        </tfoot>
    </table>
</body>

</html>
