<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('dashboard') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Cashier</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ Str::contains(Route::current()->getName(), 'dashboard') ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    @can('admin')
        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Feature
        </div>

        <!-- Nav Item - Transaksi Collapse Menu -->
        <li class="nav-item {{ Str::contains(Route::current()->getName(), 'transaksi') ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#transaksi">
                <i class="fas fa-fw fa-shopping-cart"></i>
                <span>Transaksi</span>
            </a>
            <div id="transaksi" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Transaksi</h6>
                    <a class="collapse-item" href="{{ route('transaksi.create') }}">Tambah Transaksi</a>

                    <a class="collapse-item" href="{{ route('transaksi.index') }}">Data Transaksi</a>

                </div>
            </div>
        </li>

        <!-- Nav Item - Barang Collapse Menu -->
        <li class="nav-item {{ Str::contains(Route::current()->getName(), 'barang') ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#barang">
                <i class="fas fa-fw fa-box"></i>
                <span>Barang</span>
            </a>
            <div id="barang" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Barang</h6>
                    <a class="collapse-item" href="{{ route('barang.create') }}">Tambah Barang</a>

                    <a class="collapse-item" href="{{ route('barang.index') }}">Data Barang</a>

                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Pengguna
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item {{ Str::contains(Route::current()->getName(), 'pengguna') ? 'active' : '' }}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
                aria-controls="collapsePages">
                <i class="fas fa-fw fa-users"></i>
                <span>Pengguna</span>
            </a>
            <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Pengguna</h6>
                    <a class="collapse-item" href="{{ route('pengguna.create') }}">Tambah Pengguna</a>
                    <a class="collapse-item" href="{{ route('pengguna.index') }}">Data Pengguna</a>
                </div>
            </div>
        </li>
    @endcan

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>


</ul>
