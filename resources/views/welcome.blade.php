@extends('layouts.auth')

@section('title')
    Selamat Datang !
@endsection

@section('form')
    <a class="sidebar-brand d-flex align-items-center justify-content-center text-decoration-none display-3" href="#">
        <div class="rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="mx-3">Cashier</div>
    </a>
    @if (Route::has('login'))
        <div class="text-center user">
            <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                @auth
                    <a href="{{ url('/dashboard') }}" class="btn btn-primary btn-user btn-block">Dashboard</a>
                @else
                    <a href="{{ route('login') }}" class="btn btn-primary btn-user btn-block">Log in</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}" class="btn btn-primary btn-user btn-block">Register</a>
                    @endif
                @endauth
            </div>
        </div>
    @endif
@endsection
