@extends('layouts.master')

@section('title')
    Data Pengguna | Cashier
@endsection

@push('styles')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush


@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Pengguna</h1>
        <a href="{{ route('pengguna.create') }}" class="btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-plus fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Tambah Pengguna</div>
        </a>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Data Pengguna</h6>
        </div>
        <div class="card-body">

            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Nama Pengguna</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Nama Pengguna</th>
                            <th>Email</th>
                            <th>Role</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($pengguna as $item)
                            <tr>
                                <td class="text-center">{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td class="text-center">{{ $item->is_admin ? 'Admin Kasir' : 'Kasir' }}</td>
                                <td class="text-center text-nowrap">
                                    <a href="{{ route('pengguna.show', ['pengguna' => $item->id]) }}"
                                        class="btn btn-sm btn-info">Detail</a>
                                    <button type="button" class="btn btn-sm btn-outline-primary updatePengguna"
                                        data-toggle="modal" data-target="#updatePengguna" data-id="{{ $item->id }}"
                                        data-nama="{{ $item->name }}" data-email="{{ $item->email }}"
                                        data-role="{{ $item->is_admin }}"
                                        data-url="{{ route('pengguna.update', ['pengguna' => $item->id]) }}">
                                        Edit
                                    </button>
                                    <form action="{{ route('pengguna.update', ['pengguna' => $item->id]) }}"
                                        method="POST" class="d-inline">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="updatePengguna" tabindex="-1" aria-labelledby="updatePenggunaLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content shadow mb-4">
                <div class="card">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Update Pengguna</h6>
                    </div>
                    <div class="card-body">
                        <form id="formupdatePengguna" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="namaPengguna">Nama Pengguna</label>
                                <input type="text" name="nama_pengguna" id="nama_pengguna" class="form-control"
                                    oninvalid="this.setCustomValidity('Nama Pengguna tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <div class="form-group">
                                <label for="emailPengguna">Alamat Email</label>
                                <input type="email" id="email" class="form-control"
                                    oninvalid="this.setCustomValidity('Email tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" disabled>
                            </div>
                            <div class="form-group">
                                <label for="password">Kata Sandi</label>
                                <input type="text" name="password" minlength="8" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select class="form-control" name="role" id="role" required>
                                    <option value="0">Kasir</option>
                                    <option value="1">Admin Kasir</option>
                                </select>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Update Pengguna</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.updatePengguna').on('click', function() {
                var id = $(this).attr('data-id')
                $('form#formupdatePengguna').attr('action', $(this).attr('data-url'))
                $('#nama_pengguna').val($(this).attr('data-nama'))
                $('#email').val($(this).attr('data-email'))
                $('#role').val($(this).attr('data-role'))
            })
        })
    </script>

    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: 3
                }]
            });
        });
    </script>
@endpush
