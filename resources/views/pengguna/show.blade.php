@extends('layouts.master')

@section('title')
    {{ $pengguna->name }} | Cashier
@endsection

@push('styles')
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
@endpush


@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Profil</h1>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Profil Pengguna</h6>
        </div>
        <div class="card-body text-center">
            <img src="{{ asset('img/undraw_profile.svg') }}" class="img-profile rounded-circle" style="max-width: 200px">

            <h1>{{ $pengguna->name }}</h1>
            <p>Role : {{ $pengguna->is_admin ? 'Admin Kasir' : 'Kasir' }}</p>
            <p>Email : {{ $pengguna->email }}</p>
        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="updatePengguna" tabindex="-1" aria-labelledby="updatePenggunaLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content shadow mb-4">
                <div class="card">
                    <div class="card-header py-3">
                        <h6 class="m-0 font-weight-bold text-primary">Form Update Pengguna</h6>
                    </div>
                    <div class="card-body">
                        <form id="formupdatePengguna" method="post">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="namaPengguna">Nama Pengguna</label>
                                <input type="text" name="nama_pengguna" id="nama_pengguna" class="form-control"
                                    oninvalid="this.setCustomValidity('Nama Pengguna tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <div class="form-group">
                                <label for="emailPengguna">Alamat Email</label>
                                <input type="email" name="email" id="email" class="form-control"
                                    oninvalid="this.setCustomValidity('Email tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Kata Sandi</label>
                                <input type="text" name="password" minlength="8" class="form-control"
                                    oninvalid="this.setCustomValidity('Password tidak boleh kosong')"
                                    oninput="this.setCustomValidity('')" required>
                            </div>
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select class="form-control" name="role" id="role" required>
                                    <option value="0">Kasir</option>
                                    <option value="1">Admin Kasir</option>
                                </select>
                            </div>
                            <hr>
                            <div class="text-right">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary">Update Pengguna</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.updatePengguna').on('click', function() {
                var id = $(this).attr('data-id')
                $('form#formupdatePengguna').attr('action', $(this).attr('data-url'))
                $('#nama_pengguna').val($(this).attr('data-nama'))
                $('#email').val($(this).attr('data-email'))
                $('#role').val($(this).attr('data-role'))
            })
        })
    </script>

    <!-- Page level plugins -->
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page level custom scripts -->
    <script>
        // Call the dataTables jQuery plugin
        $(document).ready(function() {
            $('#dataTable').DataTable({
                columnDefs: [{
                    orderable: false,
                    targets: 3
                }]
            });
        });
    </script>
@endpush
