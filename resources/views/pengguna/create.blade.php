@extends('layouts.master')

@section('title')
    Tambah Pengguna | Cashier
@endsection

@section('content')
    <div class="d-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Tambah Pengguna</h1>
        <a href="{{ route('pengguna.index') }}" class="btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-list fa-sm text-white-50 d-inline-block"></i>
            <div class="d-none d-sm-inline-block">Data Pengguna</div>
        </a>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Pengguna Baru</h6>
        </div>
        <div class="card-body">
            <form action="{{ route('pengguna.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="namaPengguna">Nama Pengguna</label>
                    <input type="text" name="nama_pengguna" class="form-control"
                        oninvalid="this.setCustomValidity('Nama Pengguna tidak boleh kosong')"
                        oninput="this.setCustomValidity('')" required>
                </div>
                <div class="form-group">
                    <label for="emailPengguna">Alamat Email</label>
                    <input type="email" name="email" class="form-control"
                        oninvalid="this.setCustomValidity('Email tidak boleh kosong')" oninput="this.setCustomValidity('')"
                        required>
                </div>
                <div class="form-group">
                    <label for="password">Kata Sandi</label>
                    <input type="text" name="password" minlength="8" class="form-control"
                        oninvalid="this.setCustomValidity('Password tidak boleh kosong')"
                        oninput="this.setCustomValidity('')" required>
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <select class="form-control" name="role" id="role" required>
                        <option value="0">Kasir</option>
                        <option value="1">Admin Kasir</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary w-100 mb-3">Tambah Pengguna Baru</button>
            </form>

        </div>
    </div>
@endsection
