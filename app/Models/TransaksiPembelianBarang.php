<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPembelianBarang extends Model
{
    use HasFactory;
    protected $table = 'transaksi_pembelian_barang';
    protected $fillable = ['transaksi_pembelian_id', 'master_barang_id','jumlah','harga_satuan'];
    public $timestamps = false;

    public function transaksiPembelian(){
        return $this->belongsTo(TransaksiPembelian::class);
    }

    public function masterBarang(){
        return $this->belongsTo(MasterBarang::class);
    }
}
