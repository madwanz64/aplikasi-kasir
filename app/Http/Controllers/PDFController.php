<?php

namespace App\Http\Controllers;

use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Http\Request;
use PDF;

class PDFController extends Controller
{
    public function printTransaksi($id)
    {
        $transaksi = TransaksiPembelian::find($id);
        $barang = TransaksiPembelianBarang::with(['masterBarang'])->where('transaksi_pembelian_id',$id)->get();
        $data = [
            'title' => 'Cashier',
            'transaksi' => $transaksi,
            'barang' => $barang,
        ];
          
        $pdf = PDF::loadView('myPDF', $data)->setPaper([0,0,216,300]);
    
        return $pdf->download('transaksi'.$transaksi->created_at->format('HidmY').'.pdf');
    }
}
