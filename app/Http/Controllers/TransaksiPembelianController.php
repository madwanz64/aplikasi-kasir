<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Http\Request;
use Alert;

class TransaksiPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $master_barang = MasterBarang::all();
        if(request('barang') ?? false) {
            if(request('barang')[0] != NULL) {
                $transaksi_pembelian = TransaksiPembelianBarang::all();
                $countBarang = count(request('barang'));
                for ($i=0; $i < $countBarang; $i++) { 
                    $transaksi_pembelian_id = $transaksi_pembelian->where('master_barang_id', strtok(request('barang')[$i], ' '))->sortBy('transaksi_pembelian_id')->unique('transaksi_pembelian_id')->pluck('transaksi_pembelian_id')->toArray();
                    $transaksi_pembelian = TransaksiPembelianBarang::whereIn('transaksi_pembelian_id', $transaksi_pembelian_id)->get();
                }
                $transaksi_pembelian = TransaksiPembelian::whereIn('id', $transaksi_pembelian_id);                
            }   else {
                $transaksi_pembelian = TransaksiPembelian::latest();
            }
        }else {
            $transaksi_pembelian = TransaksiPembelian::latest();
        }
         

        if(request('date')!=NULL) {
            $transaksi_pembelian = $transaksi_pembelian->where('created_at', 'like', "%".request('date')."%");
        }
        if(request('min') !=NULL) {
            $transaksi_pembelian = $transaksi_pembelian->where('total_harga', '>', request('min'));
        }
        if(request('max')!=NULL) {
            $transaksi_pembelian = $transaksi_pembelian->where('total_harga', '<', request('max'));
        }

        $transaksi_pembelian = $transaksi_pembelian->get();
        return view('transaksi.index', ['barang'=>$master_barang, 'transaksi_pembelian'=>$transaksi_pembelian]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $barang = MasterBarang::all();
        return view('transaksi.create', ['barang'=>$barang]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'barang' => 'required',
            'kuantitas' => 'required',
        ]);

        $transaksi_pembelian = new TransaksiPembelian;
        $transaksi_pembelian->total_harga = 0;
        $transaksi_pembelian->save();
        
        $count = count($request->barang);
        $barang = MasterBarang::all();
        for ($i=0; $i < $count; $i++) { 
            if($request->barang[$i] != NULL and $request->kuantitas[$i] != NULL) {
                $barangid = strtok($request->barang[$i], ' ');
                $harga_satuan = $barang->find($barangid)->harga_satuan;
                $transaksiPembelianBarang = new TransaksiPembelianBarang;
                $transaksiPembelianBarang->transaksi_pembelian_id = $transaksi_pembelian->id;
                $transaksiPembelianBarang->master_barang_id = $barangid;
                $transaksiPembelianBarang->jumlah = $request->kuantitas[$i];
                $transaksiPembelianBarang->harga_satuan = $harga_satuan;
                $transaksiPembelianBarang->save();

                $transaksi_pembelian->total_harga += $harga_satuan * $request->kuantitas[$i];
            }
        }
        $transaksi_pembelian->save();
        $url = route('transaksi.show', ['transaksi'=>$transaksi_pembelian->id]);
        alert()->html('Berhasil'," Transaksi berhasil ditambahkan, <a target='_blank' href='$url'>Lihat Transaksi</a>",'success');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransaksiPembelian  $transaksiPembelian
     * @return \Illuminate\Http\Response
     */
    public function show(TransaksiPembelian $transaksiPembelian, $id)
    {
        $transaksi_pembelian = TransaksiPembelian::findOrFail($id);
        $transaksiPembelian = TransaksiPembelianBarang::with(['masterBarang'])->where('transaksi_pembelian_id',$id)->get();
        $total_harga = 0;
        foreach ($transaksiPembelian as $key => $value) { 
            $total_harga += $value->jumlah*$value->harga_satuan;
        }
        return view('transaksi.show', ['id'=> $id,'transaksi'=>$transaksiPembelian, 'total'=>$total_harga]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TransaksiPembelian  $transaksiPembelian
     * @return \Illuminate\Http\Response
     */
    public function edit(TransaksiPembelian $transaksiPembelian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TransaksiPembelian  $transaksiPembelian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransaksiPembelian $transaksiPembelian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransaksiPembelian  $transaksiPembelian
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransaksiPembelian $transaksiPembelian)
    {
        //
    }
}
