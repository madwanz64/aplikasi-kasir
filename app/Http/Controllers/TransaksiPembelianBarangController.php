<?php

namespace App\Http\Controllers;

use App\Models\TransaksiPembelianBarang;
use Illuminate\Http\Request;

class TransaksiPembelianBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransaksiPembelianBarang  $transaksiPembelianBarang
     * @return \Illuminate\Http\Response
     */
    public function show(TransaksiPembelianBarang $transaksiPembelianBarang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TransaksiPembelianBarang  $transaksiPembelianBarang
     * @return \Illuminate\Http\Response
     */
    public function edit(TransaksiPembelianBarang $transaksiPembelianBarang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TransaksiPembelianBarang  $transaksiPembelianBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransaksiPembelianBarang $transaksiPembelianBarang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransaksiPembelianBarang  $transaksiPembelianBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransaksiPembelianBarang $transaksiPembelianBarang)
    {
        //
    }
}
