<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TransaksiPembelian;
use App\Models\TransaksiPembelianBarang;
use Auth;
use DB;

class HomeController extends Controller
{
    public function home() {
        return view('welcome');
    }
    
    public function dashboard() {

        if(auth()->user()->is_admin) {
            $todaydate = date_create('now', timezone_open('Asia/Jakarta'));
            $transaksi_pembelian = DB::table('transaksi_pembelian')->select(DB::raw('SUM(total_harga) as total_harga'), DB::raw("(DATE_FORMAT(created_at, '%d-%m-%Y')) as tanggal"))->whereYear('created_at', $todaydate->format('Y'))->whereMonth('created_at', $todaydate->format('n'))->orderBy('created_at')
            ->groupBy(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))->get();
            $transaksi_pembelian_barang = TransaksiPembelian::with(['transaksiPembelianBarang'])->whereYear('created_at', $todaydate->format('Y'))->whereMonth('created_at', $todaydate->format('n'))->orderBy('created_at', 'desc')->get();
            $total_barang = 0;
            foreach ($transaksi_pembelian_barang as $key => $value) {
                $total_barang += $value->transaksiPembelianBarang->sum('jumlah');
            }
            return view('dashboard.admin', ['transaksi_pembelian'=>$transaksi_pembelian, 'transaksi_pembelian_barang'=>$transaksi_pembelian_barang, 'total_barang'=> $total_barang]);
        } else {
            $todaydate = date_create('now', timezone_open('Asia/Jakarta'))->format('Y-m-d');
            $transaksi_pembelian = TransaksiPembelian::with(['transaksiPembelianBarang'])->where('created_at','like',$todaydate.'%')->orderBy('created_at', 'desc')->get();
            $total_barang = 0;
            foreach ($transaksi_pembelian as $key => $value) {
                $total_barang += $value->transaksiPembelianBarang->sum('jumlah');
            }
            return view('dashboard.user', ['transaksi_pembelian'=>$transaksi_pembelian, 'total_barang'=> $total_barang]);
        }
        
    }
}
