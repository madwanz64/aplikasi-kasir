<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use App\Models\TransaksiPembelianBarang;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $master_barang = MasterBarang::all();
        return view('barang.index', ['barang'=>$master_barang]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_barang' => 'required',
            'harga_satuan' => 'required|integer',
        ]);

        $master_barang = new MasterBarang;
        $master_barang->nama_barang = $request->nama_barang;
        $master_barang->harga_satuan = $request->harga_satuan;
        $master_barang->save();

        Alert::success('Berhasil', $master_barang->nama_barang." dengan harga Rp ".number_format($master_barang->harga_satuan, 0, ',', '.')." berhasil ditambahkan");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterBarang  $masterBarang
     * @return \Illuminate\Http\Response
     */
    public function show($id, MasterBarang $masterBarang)
    {
        $transaksi_pembelian_barang = TransaksiPembelianBarang::with(['transaksiPembelian'])->where('master_barang_id', $id)->get();
        $data = ['jumlah'=> 0, 'total'=> 0];
        foreach ($transaksi_pembelian_barang as $key => $value) {
            $data['jumlah'] += $value->jumlah;
            $data['total'] += $value->jumlah * $value->harga_satuan;
        }
        $master_barang = MasterBarang::find($id);
        return view ('barang.show', ['transaksi'=>$transaksi_pembelian_barang, 'data'=>$data, 'barang'=> $master_barang]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterBarang  $masterBarang
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterBarang $masterBarang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterBarang  $masterBarang
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, MasterBarang $masterBarang)
    {
        $validated = $request->validate([
            'nama_barang' => 'required',
            'harga_satuan' => 'required|integer',
        ]);
        $master_barang = MasterBarang::find($id);
        $master_barang->nama_barang = $request->nama_barang;
        $master_barang->harga_satuan = $request->harga_satuan;
        $master_barang->save();

        Alert::success('Berhasil', $master_barang->nama_barang." dengan harga Rp ".number_format($master_barang->harga_satuan, 0, ',', '.')." berhasil diedit");
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterBarang  $masterBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, MasterBarang $masterBarang)
    {
        MasterBarang::destroy($id);
        Alert::success('Berhasil', 'Data Barang berhasil dihapus');
        return redirect()->back();
    }
}
