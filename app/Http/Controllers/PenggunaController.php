<?php

namespace App\Http\Controllers;

use Hash;
use App\Models\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengguna = User::all();
        return view('pengguna.index', ['pengguna'=> $pengguna]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pengguna.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_pengguna' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8',
            'role' => 'required'
        ]);

        $user = new User;
        $user->name = $request->nama_pengguna;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->is_admin = $request->role;
        $user->save();
        
        Alert::success('Berhasil', 'Pengguna baru dengan nama pengguna '.$user->name.' berhasil ditambahkan.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id, User $user)
    {
        if(!auth()->user()->is_admin && auth()->user()->id != $id) {
            abort(403);
        } else {
            $pengguna = User::findOrFail($id);
            return view('pengguna.show', ['pengguna'=>$pengguna]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, User $user)
    {
        $validated = $request->validate([
            'nama_pengguna' => 'required',
            'role' => 'required'
        ]);

        $user = User::find($id);
        $user->name = $request->nama_pengguna;
        if($id == 1 && auth()->user()->id == 1) {
            if ($request->role != 1) {
                Alert::error('Gagal', 'Role admin kasir utama tidak dapat diubah.');
                return redirect()->back();
            } else {
                if($request->password != NULL) {
                    $user->password = Hash::make($request->password);
                }
                $user->save();
                Alert::success('Berhasil', 'Pengguna dengan nama pengguna '.$user->name.' berhasil diedit.');
                return redirect()->back();
            }
        } else {
            $user->is_admin = $request->role;
            if($request->password != NULL) {
                $user->password = Hash::make($request->password);
            }
            $user->save();
            Alert::success('Berhasil', 'Pengguna dengan nama pengguna '.$user->name.' berhasil diedit.');
            return redirect()->back();
        }       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, User $user)
    {
        if($id == 1) {
            Alert::error('Gagal', 'Admin Kasir Utama tidak dapat dihapus');
            return redirect()->back();
        } else {
            User::destroy($id);
            Alert::success('Berhasil', 'Pengguna berhasil dihapus');
            return redirect()->back();
        }
    }
}
