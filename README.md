# APLIKASI KASIR

Aplikasi kasir ini merupakan sebuah project yang dibuat dengan framework laravel 8.x. Template yang digunakan adalah template [SB Admin 2](https://startbootstrap.com/theme/sb-admin-2)

## Fitur :

-   Mencatat transaksi pembelian barang
-   Menampilkan daftar transaksi pembelian barang
-   Authentikasi login pengguna
-   Pengaturan data pengguna
-   Pengaturan data master produk

## Fitur tambahan :

-   Terdapat beberapa data tambahan dan grafik penjualan bulanan di halaman dashboard jika pengguna merupakan Admin Kasir

## Keterangan :

Selain 5 barang awal yang dibuat langsung dengan DB Seeder, terdapat juga 2 akun yang bisa langsung digunakan

## Demo :

https://tebakdiri.com/cashier/

## Akun :

-   Admin@aplikasikasir.com password
-   kasir@aplikasikasir.com password
