<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('master_barang')->insert([[
            'nama_barang' => 'Sabun Batang',
            'harga_satuan' => '3000',
        ],[
            'nama_barang' => 'Mi Instan',
            'harga_satuan' => '2000',
        ],[
            'nama_barang' => 'Pensil',
            'harga_satuan' => '1000',
        ],[
            'nama_barang' => 'Kopi Sachet',
            'harga_satuan' => '1500',
        ],[
            'nama_barang' => 'Air minum galon',
            'harga_satuan' => '20000',
        ]]);
    }
}
