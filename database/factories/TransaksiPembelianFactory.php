<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TransaksiPembelianFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'total_harga' => $this->faker->randomNumber(5,true),
            'created_at' => $this->faker->dateTimeInInterval('-3 week', '+6 week', 'Asia/Jakarta'),
        ];
    }
}
