<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TransaksiPembelianBarangFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'transaksi_pembelian_id' => $this->faker->numberBetween(1,150),
            'master_barang_id' => $this->faker->numberBetween(1,5),
            'jumlah' => $this->faker->numberBetween(1,10),
            'harga_satuan' => $this->faker->randomElement([3000, 2000, 1000, 1500, 20000])
        ];
    }
}
