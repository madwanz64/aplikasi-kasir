<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MasterBarangController;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\TransaksiPembelianController;
use App\Http\Controllers\PDFController;
use App\Models\TransaksiPembelian;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::middleware(['auth'])->group(function () {
    Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
    Route::resource('transaksi', TransaksiPembelianController::class)->only(['store','show', 'create']);
    Route::middleware(['admin'])->group(function (){
        Route::resource('transaksi', TransaksiPembelianController::class)->only(['index']);
        Route::resource('barang', MasterBarangController::class)->except(['edit']);
        Route::resource('pengguna', PenggunaController::class)->except(['show', 'edit']);
    });
    Route::resource('pengguna', PenggunaController::class)->only(['show']);
    Route::get('transaksi/{transaksi_id}/printtransaksi', [PDFController::class, 'printTransaksi'])->name('transaksi.print');
});
require __DIR__ . '/auth.php';
